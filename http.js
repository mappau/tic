var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path'); 

var getUser = function(id){
   if(fs.existsSync('data/'+id+'.json'))
  {
    var user = JSON.parse(fs.readFileSync('data/'+id+'.json', 'utf8'));
	user["currentTime"]= new Date().getTime();
	
	return user;
  }
  return {
    id: id,
    sessions: [],
	currentTime: new Date().getTime()
  }
};

var saveUser = function(user){
    fs.writeFileSync('data/'+user.id+'.json', JSON.stringify(user));
};

app.use(function (req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next()
});

app.get('/', function(req,res){
  res.status(400);
    res.type('text');
    res.send('hello to the time clock\n\nAvailable calls:\nGET /username/sessions\nGET /username/status\nPOST /username/start\nPOST /username/end');
});

app.get('/:id/sessions', function(req, res) {
  res.type('json'); // set content-type
  res.send(getUser(req.params.id));
});

app.get('/:id/status', function(req, res) {
  var user = getUser(req.params.id);
  var status = 1;
  if(user.sessions.length===0) status = 0;
  else if(user.sessions[user.sessions.length-1].end) status = 0;
  
  res.type('json');
  res.status(200);
  res.send({
    id:req.params.id,
    status: status,
	currentTime: new Date().getTime()
    }); 
});

app.post('/:id/start', function(req, res) {
  
  
  res.status(200);
  res.type('json');
  
  var user = getUser(req.params.id);
    
  if(user.sessions.length===0) 
  {
      user.sessions.push({start: new Date().getTime()});
  }
  else if(user.sessions[user.sessions.length-1].start && user.sessions[user.sessions.length-1].end)
  {
      user.sessions.push({start: new Date().getTime()});
  }else
  {
    res.status(400);
    res.type('text');
    res.send('can\'t start twice');
    res.end();
    return;
  }
  saveUser(user);
  res.send({
    id:user.id,
    session: user.sessions[user.sessions.length-1]
    
    
    });
});

app.post('/:id/end', function(req, res) {
  res.status(200);
  res.type('json');
  
  var user = getUser(req.params.id);
  
  if(user.sessions.length===0) 
  {
    res.status(400);
    res.type('text');
    res.send('no session to end');
    res.end();
    return;
  }
  
  if(user.sessions[user.sessions.length-1].end)
  {
      res.status(400);
      res.type('text');
      res.send('last session has ended already');
      res.end();
      return;
  }
  user.sessions[user.sessions.length-1].end = new Date().getTime();
  
  saveUser(user);
  
  res.send({
    id:user.id,
    session: 
      user.sessions[user.sessions.length-1]
    });
});


app.listen( 9876);
